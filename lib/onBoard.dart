import 'package:flutter/material.dart';

class OnBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/bgPic.jpg"),
                fit: BoxFit.cover)),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Colors.black.withOpacity(0.3),
            Colors.black.withOpacity(0.3)
          ])),
          child: Padding(
            padding: EdgeInsets.all(40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 100.0),
                // new Text(data)
                Text(
                  "BACKPACKS",
                  style: TextStyle(
                      fontFamily: 'Merienda Bold',
                      color: Color(0xFF00031b),
                      fontSize: 42.0,
                      fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Travel in style",
                  style: TextStyle(
                    fontFamily: 'Merienda',
                    color: Color(0xFF00031b), fontSize: 14
                    ),
                ),
                Spacer(),
              
                ButtonTheme(
                  minWidth: 400.0,
                  height: 50.0,
                  child: RaisedButton(
                   color: Color(0xFFe65b00),
                    textColor: Colors.white,  
                    onPressed: () {},
                   child: Text(
                      "Explore",
                      style: TextStyle(fontSize: 24.0),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
